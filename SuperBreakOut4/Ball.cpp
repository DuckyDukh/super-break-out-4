#include "Ball.h"

Ball::Ball()
{
	life = 3;
	speed = 100.f;
	defaultValues();

	ball.setRadius(size/2);
	ball.setFillColor(sf::Color(250, 80, 200));
	ball.setOrigin(size/2, size/2);
	ball.setPosition(x, y);
}


Ball::~Ball()
{
}

void Ball::defaultValues(){
	size = 40;
	x = 400;
	y = 450;
	vecX = 1;
	vecY = -1;
}

void Ball::update(float delta) {
	if (outOfScreen()) {
		life--;
		defaultValues();
	}
	if (y - size / 2 <= 0) {
		y = size / 2;
		vecY = -vecY;
	}
	if (x - size / 2 <= 0) {
		x = size / 2;
		vecX = -vecX;
	}
	if (x + size / 2 >= 800) {
		x = 800 - size / 2;
		vecX = -vecX;
	}

	x += speed * delta * vecX;
	y += speed * delta * vecY;
	ball.setPosition(x, y);
}

sf::CircleShape Ball::getShape() {
	return ball;
}

bool Ball::collidesWith(GameElement element) {
	return ball.getGlobalBounds().intersects(element.getShape().getGlobalBounds());
}

void Ball::reverseSpeed() {
	vecY = -vecY;
}

int Ball::getLife() {
	return life;
}

bool Ball::outOfScreen() {
	if (y - size > 700)
		return true;
	return false;
}