#include "HighScore.h"



HighScore::HighScore()
{
	inFile.open("highScore/highScore.txt", std::ifstream::in);

	if (!inFile.is_open()){
		outFile.open("highScore/highScore.txt", std::ofstream::out);
		inFile.open("highScore/highScore.txt", std::ifstream::in);

		setHighScore(0);
		outFile.close();
	}

	inFile >> highScore;
	inFile.close();
}


HighScore::~HighScore()
{
}

void HighScore::setHighScore(int score) {
	outFile.open("highScore/highScore.txt", std::ofstream::trunc);
	outFile << std::to_string(score);
}

int HighScore::getHighScore() {
	return highScore;
}