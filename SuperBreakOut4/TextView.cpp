#include "TextView.h"


TextView::TextView(int x, int y)
{
	font.loadFromFile("font/OpenSans-Regular.ttf");
	text.setFont(font);
	text.setCharacterSize(24);
	text.setFillColor(sf::Color(255, 255, 255));
	text.setPosition(x, y);
}


TextView::~TextView()
{
}

void TextView::setText(std::string _text) {
	text.setString(_text);
}

sf::Text TextView::getText() {
	return text;
}