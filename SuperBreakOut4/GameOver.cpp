#include "GameOver.h"



GameOver::GameOver()
{
	isHighScore = false;

	textrBg.loadFromFile("assets/gameOverBG.png");
	sprBg.setTexture(textrBg);

	buttonRestart.setPosition(250, 400);
	buttonRestart.create(2);
	ButtonList::getInstance().add(buttonRestart);

	scoreView = new TextView(320, 350);
	highScoreView = new TextView(100, 310);

	highScore = new HighScore();

	newRecord = new TextView(320, 310);
	newRecord->setText("NEW RECORD!");
}


GameOver::~GameOver()
{
	delete scoreView;
	delete newRecord;
	delete highScore;	
	delete highScoreView;
}

void GameOver::draw(sf::RenderWindow* window) {
	window->draw(sprBg);
	window->draw(buttonRestart.getSprite());
	window->draw(scoreView->getText());
	window->draw(highScoreView->getText());
	if (isHighScore)
		window->draw(newRecord->getText());
}

void GameOver::setScore(int score) {
	scoreView->setText("Your score: " + std::to_string(score));
	if (highScore->getHighScore() < score) {
		highScore->setHighScore(score);
		isHighScore = true;
	}
	highScoreView->setText("High score: \n" + std::to_string(highScore->getHighScore()));
}