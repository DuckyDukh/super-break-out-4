#include "Menu.h"

Menu::Menu()
{
	textrBg.loadFromFile("assets/BG.png");
	sprBg.setTexture(textrBg);

	buttonStart.setPosition(450, 300);
	buttonStart.create(play);

	buttonExit.setPosition(450, 450);
	buttonExit.create(exit);

	ButtonList::getInstance().add(buttonStart);
	ButtonList::getInstance().add(buttonExit);
}

void Menu::draw(sf::RenderWindow* window) {
	window->draw(sprBg);
	window->draw(buttonStart.getSprite());
	window->draw(buttonExit.getSprite());

}

Menu::~Menu() {

}
