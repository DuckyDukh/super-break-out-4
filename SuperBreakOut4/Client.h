#pragma once
#include <SFML/Graphics.hpp>
#include "Menu.h"
#include "BreakOut.h"
#include "ButtonList.h"
#include "GameOver.h"

class Client
{
private: 
	enum state { inMenu, inGame, gameOver};
	enum event { left, right };
	sf::RenderWindow *window;
	Menu *menu;
	BreakOut *game;
	GameOver *gameOverScreen;
	sf::Clock clock;
	int state;
	float delta;
public:
	Client();
	~Client();
	void start();
};

