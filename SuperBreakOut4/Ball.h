#pragma once
#include <SFML/Graphics.hpp>
#include "GameElement.h"

class Ball
{
private:
	sf::CircleShape ball;
	float vecX, vecY, speed;
	int size, x, y, life;
	bool outOfScreen();
	void defaultValues();
public:
	Ball();
	~Ball();
	bool collidesWith(GameElement);
	void update(float);
	sf::CircleShape getShape();
	int getLife();
	void reverseSpeed();
};

