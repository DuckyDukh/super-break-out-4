#include "ButtonList.h"


ButtonList &ButtonList::getInstance() {
	static ButtonList instance;
	return instance;
}

ButtonList::ButtonList() {
}

ButtonList::~ButtonList(){
}

void ButtonList::add(Button button) {
	buttonList.push_back(button);
}

std::vector<Button> ButtonList::getList(){
	return buttonList;
}
