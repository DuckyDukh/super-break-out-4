#pragma once
#include <SFML/Graphics.hpp>
#include "GameElement.h"

class Player: public GameElement
{
private:
	float speed;
	bool toLeft, toRight;
public:
	Player(int, int);
	~Player();
	void goRight();
	void goLeft();
	void update(float);
};

