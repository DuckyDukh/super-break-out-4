#include "TargetList.h"


TargetList::TargetList()
{
}

TargetList::~TargetList()
{
}

void TargetList::add(Target target) {
	targetList.push_back(target);
}

std::vector<Target> TargetList::getList() {
	return targetList;
}

void TargetList::clear() {
	targetList.clear();
}

void TargetList::deleteTarget(int index) {
	std::vector<Target>::iterator iter = targetList.begin() + index;

	targetList.erase(iter);
}

int TargetList::getTargetNum() {
	return targetList.size();
}