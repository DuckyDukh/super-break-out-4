#include "GameElement.h"


GameElement::GameElement(int x, int y)
{
	this->x = x;
	this->y = y;

	object.setPosition(this->x, this->y);
}


GameElement::~GameElement()
{
}


sf::RectangleShape GameElement::getShape() {
	return object;
}