#pragma once
#include <vector>
#include "Button.h"

class ButtonList
{
private:
	std::vector<Button> buttonList;
	ButtonList();
public:
	static ButtonList &getInstance();
	void add(Button);
	std::vector<Button> getList();
	~ButtonList();
};

