#include "Player.h"


Player::Player(int x, int y) : GameElement(x, y)
{
	speed = 500.f;

	lenght = 150;
	width = 30;

	object.setOrigin(lenght / 2, width / 2);
	object.setSize(sf::Vector2f(lenght, width));
	object.setFillColor(sf::Color(255, 255, 255));

	toLeft = false;
	toRight = false;
}

Player::~Player() {

}

void Player::goLeft(){
	if (x - lenght / 2 >= 0)
		toLeft = true;
}

void Player::goRight() {
	if (x + lenght / 2 <= 800)
		toRight = true;
}

void Player::update(float delta) {
	if (toLeft) {
		x -= speed * delta;
		toLeft = false;
	}	
	if (toRight) {
		x += speed * delta;
		toRight = false;
	}

	object.setPosition(x, y);
}
