#pragma once
#include <SFML/Graphics.hpp>
#include <string>

class TextView
{
private:
	sf::Font font;
	sf::Text text;
public:
	TextView(int, int);
	~TextView();
	void setText(std::string);
	sf::Text getText();
};

