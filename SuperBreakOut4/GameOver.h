#pragma once
#include <SFML/Graphics.hpp>
#include "TextView.h"
#include "ButtonList.h"
#include "HighScore.h"

class GameOver
{
private:
	bool isHighScore;
	HighScore *highScore;
	TextView *newRecord;
	TextView *highScoreView;
	TextView *scoreView;
	sf::Texture textrBg;
	sf::Sprite sprBg;
	Button buttonRestart;
public:
	GameOver();
	~GameOver();
	void draw(sf::RenderWindow*);
	void setScore(int);
};

