#pragma once
#include <fstream>
#include <string>

class HighScore
{
private:
	int highScore;
	std::ofstream outFile;
	std::ifstream inFile;
public:
	HighScore();
	~HighScore();
	void setHighScore(int);
	int getHighScore();
};

