#include "Button.h"


Button::Button()
{
}


Button::~Button()
{
}

void Button::setPosition(int x, int y) {
	this->x = x;
	this->y = y;
}

void Button::create(int id) {
	this->id = id;
	switch (id) {
	case(0): 
		path = "assets/playButton.png";
		break;
	case(1):
		path = "assets/exitButton.png";
		break;
	case(2):
		path = "assets/restartButton.png";
		break;
	}

	textrButton.loadFromFile(path);
	sprButton.setTexture(textrButton);
	sprButton.setPosition(x, y);
}

sf::Sprite Button::getSprite() {
	return sprButton;
}


int Button::getId() {
	return id;
}