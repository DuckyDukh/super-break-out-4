#pragma once
#include <vector>
#include "Target.h"

class TargetList
{
private:
	std::vector<Target> targetList;
public:
	void add(Target);
	std::vector<Target> getList();
	void clear();
	void deleteTarget(int);
	int getTargetNum();
	TargetList();
	~TargetList();
};

