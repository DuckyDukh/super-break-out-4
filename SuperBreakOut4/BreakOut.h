#pragma once
#include <SFML/Graphics.hpp>
#include "Ball.h"
#include "Player.h"
#include "TargetList.h"
#include "TextView.h"

class BreakOut
{
private:
	int score;
	TextView *scoreView;
	TextView *lifeView;
	Ball *ball;
	Player *player;
	sf::Texture textrGameBg;
	sf::Sprite sprGameBg;
	TargetList *targetList;
public:
	BreakOut();
	~BreakOut();
	void draw(sf::RenderWindow*);
	void update(float);
	void start();
	void eventHandle(int);
	void generateTargets();
	bool gameOver();
	int getScore();
};

