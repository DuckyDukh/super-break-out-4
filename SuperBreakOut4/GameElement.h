#pragma once
#include <SFML/Graphics.hpp>

class GameElement
{
protected:
	int lenght, width, x, y;
	sf::RectangleShape object;
public:
	GameElement(int, int);
	~GameElement();
	sf::RectangleShape getShape();
};

