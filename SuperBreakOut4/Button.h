#pragma once
#include <SFML/Graphics.hpp>
#include <string>

class Button
{
private:
	sf::Texture textrButton;
	sf::Sprite sprButton;
	std::string path;
	int x, y;
	int id;

public:
	Button();
	~Button();
	void setPosition(int, int);
	void create(int);
	sf::Sprite getSprite();
	int getId();
};