#include "Client.h"

Client::Client()
{
	window = new sf::RenderWindow(sf::VideoMode(800, 600), "Super Break Out 4 Ultimate");
	window->setVerticalSyncEnabled(true);

	menu = new Menu();

	state = state::inMenu;
}


Client::~Client()
{
	delete window;
}

void Client::start() {

	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window->close();

			if (state == state::inGame) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
					game->eventHandle(event::left);

				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
					game->eventHandle(event::right);
			}

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
				if (state == state::inMenu) {
					for (Button button : ButtonList::getInstance().getList()) {
						if (button.getSprite().getGlobalBounds().contains((sf::Vector2f(sf::Mouse::getPosition(*window)))))
							switch (button.getId()) {
							case(0):
								game = new BreakOut();
								game->start();

								state = state::inGame;

								delete menu;
								break;
							case(1):
								window->close();
								break;								
							}
					}
				}

				if (state == state::gameOver)
					if (ButtonList::getInstance().getList()[2].getSprite().getGlobalBounds().contains((sf::Vector2f(sf::Mouse::getPosition(*window))))) {
						game = new BreakOut();
						game->start();

						state = state::inGame;

						delete gameOverScreen;
						break;
					}
			}
		}
		window->clear();

		delta = clock.restart().asSeconds();

		switch (state){
			case(state::inMenu): 		
				menu->draw(window);
				break;
			case(state::inGame):
				game->draw(window);
				game->update(delta);

				if (game->gameOver()) {
					state = gameOver;
					gameOverScreen = new GameOver();
					gameOverScreen->setScore(game->getScore());
					delete game;
				}
				break;
			case(state::gameOver):
				gameOverScreen->draw(window);
				break;
		}

		window->display();
	}
}
