#pragma once
#include <SFML/Graphics.hpp>
#include "ButtonList.h"

class Menu
{
private:
	enum { play, exit };
	sf::Texture textrBg;
	sf::Sprite sprBg;

	Button buttonStart;
	Button buttonExit;

public:
	Menu();
	~Menu();
	void draw(sf::RenderWindow*);
};

