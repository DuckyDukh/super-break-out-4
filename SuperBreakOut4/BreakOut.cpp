#include "BreakOut.h"

BreakOut::BreakOut()
{
	textrGameBg.loadFromFile("assets/gameBG.jpg");
	sprGameBg.setTexture(textrGameBg);
}


BreakOut::~BreakOut()
{
	delete ball;
	delete player;
	delete scoreView;
	delete lifeView;
	delete targetList;
}

void BreakOut::start() {
	score = 0;
	ball = new Ball();
	player = new Player(400, 550);
	scoreView = new TextView(10, 10);
	lifeView = new TextView(150, 10);

	generateTargets();
}

void BreakOut::draw(sf::RenderWindow* window) {
	window->draw(sprGameBg);
	window->draw(ball->getShape());
	window->draw(player->getShape());

	for (Target target : targetList->getList())
		window->draw(target.getShape());

	window->draw(scoreView->getText());
	window->draw(lifeView->getText());
}

void BreakOut::update(float delta) {
	ball->update(delta);
	player->update(delta);

	if (ball->collidesWith(*player))
		ball->reverseSpeed();

	for (int _i = 0; _i < targetList->getList().size(); _i++) {
		Target target = targetList->getList()[_i];
			if (ball->collidesWith(target)) {
				score += 50;
				targetList->deleteTarget(_i);
				ball->reverseSpeed();
			}
	}

	scoreView->setText("Score: " + std::to_string(score));
	lifeView->setText("Lives: " + std::to_string(ball->getLife()));
}

void BreakOut::eventHandle(int eventId) {
	switch (eventId)
	{
	case (0): 
		player->goLeft();
		break;
	case (1):
		player->goRight();
		break;
	default:
		break;
	}
}

void BreakOut::generateTargets() {
	targetList = new TargetList();
	int _x = 0, _y = 0;

	for (int _i = 0; _i < 4; _i++) {
		_x = 0;
		_y += 60;
		for (int _j = 0; _j < 4; _j++) {
			_x += 160;
			Target *target = new Target(_x, _y);
			targetList->add(*target);
		}
	}
}

bool BreakOut::gameOver() {
	if (ball->getLife() <= 0 || targetList->getTargetNum() == 0)
		return true;
	return false;
}

int BreakOut::getScore() {
	return score;
}
